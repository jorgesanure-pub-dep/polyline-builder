## 0.0.1
- Polyline builder
## 0.0.7
- Provide distance(m) & duration(s)
## 0.0.8
- Polyline fit padding
## 0.0.9
- Provide polyline geometry
## 0.0.10
- get polyline from locations if geometry is empty