import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:polyline_builder/src/providers/models/polilyne_model.dart';

import 'src/components/loading_widget.dart';
import 'src/providers/polyline_provider.dart';

class PolylineBuilder extends StatelessWidget {
  final Widget Function(
      Set<Polyline>, double distance, double duration, String geometry) builder;
  // To update camera zoom to display whole polyline on map
  final GoogleMapController controller;

  final List<LatLng> locations;

  final double polylineFitPadding;

  static String MAPBOX_ACCESS_TOKEN;

  final String polylinesGeometry;

  const PolylineBuilder(
      {Key key,
      @required this.builder,
      this.locations,
      this.controller,
      this.polylineFitPadding,
      this.polylinesGeometry})
      : assert(builder != null),
        assert(
            (locations ?? const []).length >= 2 ||
                (polylinesGeometry ?? '').length > 0,
            "'locations' must be at least 2 length or 'polylinesGeometry' can't be blank"),
        super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            // Map
            FutureBuilder<PolylineModel>(
              future:
                  PolylineProvider.getPolylines(locations, polylinesGeometry),
              builder: (context, snapshot) {
                final polylineModel = snapshot.data;

                final polylines =
                    polylineModel == null ? null : {polylineModel.polilyne};

                if (controller != null)
                  _fitCameraToPolyline(controller, polylines);

                return Stack(
                  children: [
                    builder(polylines, polylineModel?.distance,
                        polylineModel?.duration, polylineModel?.geometry),
                    Visibility(
                        visible:
                            snapshot.connectionState == ConnectionState.waiting,
                        child: Container(
                          color: Colors.white.withOpacity(0.5),
                          alignment: Alignment.center,
                          child: LoadingWidget(),
                        ))
                  ],
                );
              },
            ),
          ],
        ),
      );

  void _fitCameraToPolyline(GoogleMapController controller, Set<Polyline> p) {
    if (p == null) return;

    double minLat = p.first.points.first.latitude;
    double minLong = p.first.points.first.longitude;
    double maxLat = p.first.points.first.latitude;
    double maxLong = p.first.points.first.longitude;
    p.forEach((poly) {
      poly.points.forEach((point) {
        if (point.latitude < minLat) minLat = point.latitude;
        if (point.latitude > maxLat) maxLat = point.latitude;
        if (point.longitude < minLong) minLong = point.longitude;
        if (point.longitude > maxLong) maxLong = point.longitude;
      });
    });
    controller.moveCamera(CameraUpdate.newLatLngBounds(
        LatLngBounds(
            southwest: LatLng(minLat, minLong),
            northeast: LatLng(maxLat, maxLong)),
        polylineFitPadding ?? 20));
  }
}
