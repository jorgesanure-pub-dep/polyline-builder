import 'package:flutter/material.dart';

class LoadingWidget extends Widget {
  @override
  Element createElement() => const CircularProgressIndicator(
        valueColor: const AlwaysStoppedAnimation<Color>(Colors.amber),
        strokeWidth: 1,
      ).createElement();
}
