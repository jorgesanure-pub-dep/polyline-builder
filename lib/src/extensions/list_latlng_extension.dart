import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:polyline_builder/src/providers/models/polilyne_model.dart';
import 'package:list_chunker/list_chunker.dart';
import 'package:polyline_builder/src/services/api_mapbox/polyline_list_service/models/polyline_list_response_model.dart';
import 'package:polyline_builder/src/services/api_mapbox/polyline_list_service/polyline_list_service.dart';
import 'package:polyline_builder/src/extensions/string_extension.dart';

extension GoogleMapPolylineListLatLngExtension on List<LatLng> {
  // To Google Map Polyline
  Future<PolylineModel> toGoogleMapPolylineSet() async {
    final PolylineListService _polylineService = PolylineListService.create();

    List<Future> futures = [];

    List waypoinst = this.chunk(25); // 25 is max allowed by mapbox api

    // Ensure last one has at least 2 locations
    if (waypoinst.last.length == 1)
      waypoinst.last.insert(0, waypoinst[waypoinst.length - 2].last);

    for (var i = 0; i < waypoinst.length; i++) {
      futures.add(_polylineService.getPolylines(
          PolylineListService.buildCoordinatesFromLatLng(waypoinst[i]),
          'es',
          false,
          'polyline6',
          true));
    }

    final List responses = await Future.wait(futures);

    PolylineModel polylineModel;

    responses?.forEach((e) {
      final response = e.body as PolylineListResponseModel;
      double distance = 0;
      double duration = 0;

      final r = response.routes.first;
      distance += r.distance;
      duration += r.duration;

      polylineModel = PolylineModel(
        geometry: r.geometry,
        polilyne: r.geometry.toGoogleMapPolylineSet(),
        distance: distance,
        duration: duration,
      );
    });

    return polylineModel;
  }
}
