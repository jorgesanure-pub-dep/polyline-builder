import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:polyline/polyline.dart' as Poly;

extension GoogleMapPolylineStringExtension on String {
  // To Google Map Polyline
  Polyline toGoogleMapPolylineSet() {
    // Decode geometry to get list of latitude & longitude
    final points =
        Poly.Polyline.Decode(encodedString: this, precision: 6).decodedCoords;
    // Parse to list of LatLng
    final List<LatLng> latLngList =
        points.map((point) => LatLng(point[0], point[1])).toList();
    // Create random polyline id
    final polylineId = PolylineId(DateTime.now().microsecond.toString());
    // Create polyline instance
    final polyline = Polyline(
        polylineId: polylineId,
        points: latLngList,
        width: 5,
        color: Color(0xFF669DF6));
    // Return set of polyline
    return polyline;
  }
}
