import 'package:google_maps_flutter/google_maps_flutter.dart';

class PolylineModel {
  final Polyline polilyne;
  final String geometry;
  final double distance;
  final double duration;

  PolylineModel({this.geometry, this.polilyne, this.distance, this.duration});
}
