import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:polyline_builder/src/services/api_mapbox/polyline_list_service/polyline_list_service.dart';
import 'package:polyline_builder/src/extensions/string_extension.dart';
import 'package:polyline_builder/src/extensions/list_latlng_extension.dart';
import 'models/polilyne_model.dart';

class PolylineProvider {
  static final PolylineListService _polylineService =
      PolylineListService.create();

  static Future<PolylineModel> getPolylines(
      List<LatLng> locations, String polylinesGeometry) async {
    PolylineModel _polylineModel;
    if ((polylinesGeometry ?? '').isNotEmpty)
      _polylineModel =
          PolylineModel(polilyne: polylinesGeometry.toGoogleMapPolylineSet());
    else if ((locations ?? []).length >= 2)
      _polylineModel = await locations.toGoogleMapPolylineSet();

    return _polylineModel;
  }
}
