// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'polyline_list_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$PolylineListService extends PolylineListService {
  _$PolylineListService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = PolylineListService;

  @override
  Future<Response<dynamic>> getPolylines(String coordinates, String language,
      bool steps, String geometries, bool alternatives) {
    final $url = '/mapbox/driving/$coordinates';
    final $params = <String, dynamic>{
      'language': language,
      'steps': steps,
      'geometries': geometries,
      'alternatives': alternatives
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }
}
