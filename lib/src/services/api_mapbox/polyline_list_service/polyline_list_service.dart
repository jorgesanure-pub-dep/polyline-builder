import 'package:chopper/chopper.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../converters/model_converter.dart';
import '../interceptors/token_interceptor.dart';
import 'models/polyline_list_response_model.dart';

part 'polyline_list_service.chopper.dart';

@ChopperApi()
abstract class PolylineListService extends ChopperService {
  static final _baseUrl = 'https://api.mapbox.com/directions/v5';

  @Get(path: '/mapbox/driving/{coordinates}')
  Future<Response> getPolylines(
    @Path() String coordinates,
    @Query('language') String language, // eg. 'es'
    @Query('steps') bool steps, // eg. false
    @Query('geometries') String geometries, // eg. 'polyline6'
    @Query('alternatives') bool alternatives, // eg. true
  );

  static PolylineListService create() {
    final client = ChopperClient(
      baseUrl: _baseUrl,
      interceptors: [TokenInterceptor(), HttpLoggingInterceptor()],
      services: [
        _$PolylineListService(),
      ],
      converter: ModelConverter<PolylineListResponseModel>(
          fromJson: (mapData) => PolylineListResponseModel.fromJson(mapData)),
      errorConverter: JsonConverter(),
    );
    return _$PolylineListService(client);
  }

  static String buildCoordinatesFromLatLng(List<LatLng> coordinates) =>
      coordinates == null
          ? null
          : coordinates.map((e) => '${e.longitude},${e.latitude}').join(';');
}
