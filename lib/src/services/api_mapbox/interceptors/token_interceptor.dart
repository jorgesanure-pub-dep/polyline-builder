import 'dart:async';
import 'package:chopper/chopper.dart';

import '../../../../polyline_builder.dart';

class TokenInterceptor implements RequestInterceptor {
  static const String ACCESS_TOKEN = "access_token";
  static String ACCESS_TOKEN_VALUE = PolylineBuilder.MAPBOX_ACCESS_TOKEN;

  @override
  FutureOr<Request> onRequest(Request request) async {
    Request newRequest = request.copyWith(
        parameters: {...request.parameters, ACCESS_TOKEN: ACCESS_TOKEN_VALUE});
    return newRequest;
  }
}
