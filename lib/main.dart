import 'package:flutter/material.dart';
import 'package:polyline_builder/polyline_builder_demo.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Polyline Builder Demo',
      home: PolylineBuilderDemo(),
    );
  }
}
